const fs = require('fs');
const path = require('path');
const loaderUtils = require("loader-utils");
const HEADING = '@import "~flexkit-vue/src/styles/_variables.less";';

const parseLess = sheet => {
    let lessVars = {};

    const sheetList = sheet.match(/@(.*:[^;]*)|@import[\s+]?["|'](.*)["|'][^;]?/gi);

    if (sheetList) {
        sheetList.forEach(variable => {
            if (/@import/i.test(variable)) {
                const lessFile = variable.match(/@import[\s+]?["|'](.*)["|'][^;]?/i)[1];
                const childrenSheet = fs.readFileSync(`./src/styles/${lessFile}`, "utf-8");

                lessVars = Object.assign(lessVars, parseLess(childrenSheet));
            } else {
                const definition = variable.split(/\s*:\s*/);
                const varName = definition[0].replace('@', '');
                const varValue = definition[1];

                if (varValue === 'true') {
                    lessVars[varName] = true;
                } else if (varValue === 'false') {
                    lessVars[varName] = false;
                } else {
                    lessVars[varName] = varValue;
                }
            }
        });
    }

    return lessVars;
};

module.exports = function (source) {
    if (this.cacheable) {
        this.cacheable();
    }

    const query = loaderUtils.parseQuery(this.query);
    const isVar = (query.isVar || false);

    const options = this.options.flexkit || {};
    const themeName = options.theme;

    let theme = '';

    if (themeName) {
        const themePath = path.resolve(themeName);

        this.addDependency(themePath);

        theme = fs.readFileSync(themePath, "utf-8");
    }

    if (isVar) {
        const themeVar = parseLess(theme);
        const sourseVar = parseLess(source);

        return "module.exports = " + JSON.stringify(Object.assign(sourseVar, themeVar));
    } else {
        return HEADING + '\n' + theme + '\n' + source;
    }
};

